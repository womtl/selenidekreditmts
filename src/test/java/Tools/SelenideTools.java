package Tools;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class SelenideTools {

    public SelenideTools clickButton(By CLICK_BUTTON) {
        $(CLICK_BUTTON).shouldBe(visible);
        $(CLICK_BUTTON).click();
        return this;
    }
    public SelenideTools sendKeysButton(By SENDKEYS_BUTTON, String key) {
        $(SENDKEYS_BUTTON).shouldBe(visible);
        $(SENDKEYS_BUTTON).clear();
        $(SENDKEYS_BUTTON).sendKeys(key);
        return this;
    }


}
