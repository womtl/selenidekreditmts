package Pages;

import Config.ProjectConfig;
import com.codeborne.selenide.Selenide;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class HomePage {

    private static final ProjectConfig cfg = ConfigFactory.create(ProjectConfig.class);

    private final By CREDITY = By.xpath("(//*[@class='sc-jJEKmz ejANpH'])[3]");

    public HomePage openHomePage() {
        Selenide.open(cfg.baseUrl());
        return this;
    }

    public HomePage credity() {
        $(CREDITY).shouldBe(visible);
        $(CREDITY).click();
        return this;
    }

}
