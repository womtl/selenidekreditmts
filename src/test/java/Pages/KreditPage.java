package Pages;

import Data.RandomData.RandomPhoneNumber;
import Data.UserData;
import Tools.SelenideTools;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;


public class KreditPage {

    private final By BUTTON_RASCHITAT_KREDIT = By.xpath("(//*[@class='Wrapper-sc-1vydk7-0 jLWLXd ButtonText-sc-48arcs-2 ivMpRV'])[2]");

    private final By SUMMA_KREDITA = By.xpath("(//*[@data-testid='input-slider'])[1]");

    private final By SROK_KREDITA = By.xpath("(//*[@data-testid='input-slider'])[2]");

    private final By AUTO = By.xpath("(//*[@class='Wrapper-sc-1vydk7-0 qtifC Label-sc-1uyl36s-2 gPjQZi'])[2]");

    private final By TSEL_KREDITA = By.xpath("(//*[@class='TextareaWrapper-sc-1ux9qvi-0 bUIboz'])[1]");

    private final By FIO = By.xpath("//*[@name='clientFio']");

    private final By DATA_ROGDENIA = By.xpath("//*[@name='birthDate']");

    private final By PHONE_NUMBER = By.xpath("//*[@name='phoneNumber']");

    private final By EMAIL = By.xpath("//*[@type='email']");

    private final By BUTTON_DALEE = By.xpath("//*[@class='Wrapper-sc-48arcs-1 cMfwrv']");

    private final By ASSERT_EMAIL = By.xpath("//*[@class='Wrapper-sc-1vydk7-0 OlnRe HelperText-sc-jsokzo-0 hByJHf']");

    public KreditPage oformlenieKredita () {
        UserData data = new UserData("23.08.1987","gggmail","Петров Петр Сергеевич","36","1000000");
        String randNumber = RandomPhoneNumber.tsifri();
        SelenideTools selTools = new SelenideTools();
        selTools.clickButton(BUTTON_RASCHITAT_KREDIT);
        selTools.sendKeysButton(SUMMA_KREDITA, data.getSum());
        selTools.sendKeysButton(SROK_KREDITA, data.getSrk());
        selTools.clickButton(TSEL_KREDITA);
        selTools.clickButton(AUTO);
        selTools.sendKeysButton(FIO, data.getFio());
        selTools.sendKeysButton(DATA_ROGDENIA, data.getDate());
        selTools.sendKeysButton(PHONE_NUMBER, randNumber);
        selTools.sendKeysButton(EMAIL, data.getMail());
        selTools.clickButton(BUTTON_DALEE);
        return this;
    }

    public KreditPage failureOfferCreditIncorrectEmail() {
        $(ASSERT_EMAIL).shouldHave(text("Введите верный электронный адрес"));
        return this;
    }

}
