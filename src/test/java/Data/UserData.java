package Data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class UserData {
    private String date;

    private String mail;

    private String fio;

    private String srk;

    private String sum;

}
