package Config;

import org.aeonbits.owner.Config;
@Config.Sources("classpath:main/resources/local.properties")
public interface ProjectConfig extends Config{
    @DefaultValue("https://www.mtsbank.ru")
    String baseUrl();


}
